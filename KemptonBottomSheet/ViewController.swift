//
//  ViewController.swift
//  KemptonBottomSheet
//
//  Created by Justin Kempton on 9/18/20.
//

import UIKit
import KemptonBottomSheetFramework

class ViewController: UIViewController, BottomSheetDelegate {
  private let bottomSheetButton = UIButton(type: .system)
  private var bottomSheet: BottomSheet? = nil
  private var toggleBottomSheet = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    //Developer created UI, this doesn't have to be a button
    bottomSheetButton.translatesAutoresizingMaskIntoConstraints = false
    bottomSheetButton.setTitle("Toggle BottomSheet", for: .normal)
    bottomSheetButton.backgroundColor = UIColor.red
    bottomSheetButton.setTitleColor(.white, for: .normal)
    bottomSheetButton.layer.cornerRadius = 20
    bottomSheetButton.addTarget(self, action: #selector(ViewController.BottomSheetButtonTapped), for: .touchUpInside)
    
    view.addSubview(bottomSheetButton)
    
    NSLayoutConstraint.activate([
      bottomSheetButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 200),
      bottomSheetButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
      bottomSheetButton.widthAnchor.constraint(equalToConstant: 250),
      bottomSheetButton.heightAnchor.constraint(equalToConstant: 40)
    ])
    
    //Create a BottomSheet instance with the optional parameters.  A BottomSheet can be customized to however the developer wants.
    bottomSheet = BottomSheet(title: "My Custom Title",
                              subTitle: "Some more information here",
                              gradient: (colorOne: .green, colorTwo: .magenta),
                              dismissButtonTapCompletion: { [weak self] in
      guard let self = self else { return }
      self.toggleBottomSheet = false
      self.bottomSheet?.dismissBottomSheet()
    })
    //Set our delegate if the user wants to track the current state of the BottomSheet
    bottomSheet?.delegate = self
    
    
    
    //This is some generic fake UI, that will represent what the developer wants to add to the BottomSheets scrollable area
    //Example 1
    let labelOne = UILabel(frame: .zero)
    labelOne.text = "Label One Pinned Top Left"
    labelOne.translatesAutoresizingMaskIntoConstraints = false
    labelOne.backgroundColor = .green
    
    let genericView = UIView(frame: .zero)
    genericView.translatesAutoresizingMaskIntoConstraints = false
    genericView.backgroundColor = .systemPurple
    
    let labelTwo = UILabel(frame: .zero)
    labelTwo.text = "Label Two Pinned Bottom Left"
    labelTwo.translatesAutoresizingMaskIntoConstraints = false
    labelTwo.backgroundColor = .yellow
    
    let labelThree = UILabel(frame: .zero)
    labelThree.text = "Label Three Pinned Top Right"
    labelThree.translatesAutoresizingMaskIntoConstraints = false
    labelThree.backgroundColor = .systemPink
    
    //Example 2
    let labelFour = UILabel(frame: .zero)
    labelFour.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem."
    labelFour.translatesAutoresizingMaskIntoConstraints = false
    labelFour.numberOfLines = 0
    
    //Add all our generic views created to the contentContainer that is inside the ScrollView
    //Example 1
    bottomSheet?.contentContainer.addSubview(labelOne)
    bottomSheet?.contentContainer.addSubview(genericView)
    bottomSheet?.contentContainer.addSubview(labelTwo)
    bottomSheet?.contentContainer.addSubview(labelThree)
    
    //Example 2
//    bottomSheet?.contentContainer.addSubview(labelFour)
//    bottomSheet?.isHorizontalScrollingEnabled = false
    
    //The developer will have to a solid understanding of how ScrollViews work with auto contraints.  This is especially true if the developer wishes to use
    //both vertical and horizontal scrolling.  In most cases, a piece of UI like this BottomSheet will only need to allow for vertical scrolling, so the developer
    //might not have to worry about making sure these items are pinned on all four corners.  In this supplied example, both verical and horizontal scrolling is
    //happening.  Try out some other more simpler UI by turning off Example 1 views and turning on Example 2 views above and below
    if let bottomSheet = bottomSheet {
      NSLayoutConstraint.activate([
        //Example 1
        labelOne.leadingAnchor.constraint(equalTo: bottomSheet.contentContainer.leadingAnchor, constant: 20),
        labelOne.topAnchor.constraint(equalTo: bottomSheet.contentContainer.topAnchor, constant: 20),

        genericView.widthAnchor.constraint(equalToConstant: 700),
        genericView.heightAnchor.constraint(equalToConstant: 2000),
        genericView.trailingAnchor.constraint(equalTo: bottomSheet.contentContainer.trailingAnchor),
        genericView.topAnchor.constraint(equalTo: labelOne.bottomAnchor, constant: 20),
        genericView.centerXAnchor.constraint(equalTo: bottomSheet.contentContainer.centerXAnchor),

        labelTwo.leadingAnchor.constraint(equalTo: bottomSheet.contentContainer.leadingAnchor, constant: 20),
        labelTwo.topAnchor.constraint(equalTo: genericView.bottomAnchor, constant: 20),
        labelTwo.bottomAnchor.constraint(equalTo: bottomSheet.contentContainer.bottomAnchor, constant: -40),

        labelThree.leadingAnchor.constraint(equalTo: labelOne.trailingAnchor, constant: 30),
        labelThree.trailingAnchor.constraint(equalTo: bottomSheet.contentContainer.trailingAnchor, constant: -20),
        labelThree.topAnchor.constraint(equalTo: bottomSheet.contentContainer.topAnchor, constant: 20),
        
        //Example 2
//        labelFour.leadingAnchor.constraint(equalTo: bottomSheet.contentContainer.leadingAnchor, constant: 20),
//        labelFour.topAnchor.constraint(equalTo: bottomSheet.contentContainer.topAnchor, constant: 20),
//        labelFour.bottomAnchor.constraint(equalTo: bottomSheet.contentContainer.bottomAnchor, constant: -20),
//        labelFour.trailingAnchor.constraint(equalTo: bottomSheet.contentContainer.trailingAnchor, constant: -20),
      ])
    }
 
  }
  
  func currentState(_ state: BottomSheetState) {
    //mointor the state of the BottomSheet here
  }
  
  @objc func BottomSheetButtonTapped(sender: UIButton) {
    //Developer implementing BottomSheet will need to handle it's instance, when its added and removed
    !toggleBottomSheet ? bottomSheet?.presentBottomSheet() : bottomSheet?.dismissBottomSheet()
    toggleBottomSheet = !toggleBottomSheet
  }


}

